import os
import tornado.web
from sqlalchemy.orm import sessionmaker, scoped_session
import wtforms_json


from model.db import engine, Base as models_base
from model import init_model
from urls import url_patterns



class Application(tornado.web.Application):
    _routes = url_patterns
    def __init__(self):
        settings = dict(
            cookie_secret="bZJc2sWbQLKos6GkHn/VB9oXwQt8S0R0kRvJ5/xJ89E=",
            login_url="/auth/login",
            debug=False,
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static")
        )

        self.db = scoped_session(sessionmaker(bind=engine))

        super(Application, self).__init__(url_patterns, **settings, autoreload=True)

    def create_database(self):
         models_base.metadata.create_all(engine)
    
    def create_admin(self):
        from model.user import User
        from configure import UserRole, EMAIL_ADMIN, DEFAULT_PASSWORD_ADMIN
        from uitls.hash_uitl import hash_password
        user_admin = self.db.query(User).filter(User.role == UserRole.ADMIN).first()
        if not user_admin:
            user_admin = User(
                role=UserRole.ADMIN,
                user_name='admin',
                email=EMAIL_ADMIN,
                password=hash_password(DEFAULT_PASSWORD_ADMIN)
            )
            self.db.add(user_admin)
            self.db.commit()
        return True


if __name__ == '__main__':
    app = Application()
    app.listen(8889)
    wtforms_json.init()
    app.create_database()
    app.create_admin()
    tornado.ioloop.IOLoop.instance().start()