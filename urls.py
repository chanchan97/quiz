import tornado
from module.auth import Register, Login, AuthLogoutHandler
from module.home import QuestionSetHandler
from module.question_set import QuestionSet, DetailQuestionSet
from module.question import Add
from module.user import UserQuestion

url_patterns = [
    tornado.web.url(r"/register", Register),
    tornado.web.url(r"/login", Login),
    tornado.web.url(r"/logout", AuthLogoutHandler),
    tornado.web.url(r"/home", QuestionSetHandler),
    tornado.web.url(r"/add_question_set", QuestionSet),
    tornado.web.url(r"/add_question/([^/]+)", Add),
    tornado.web.url(r"/detail/([^/]+)", DetailQuestionSet),
    tornado.web.url(r"/([^/]+)/([^/]+)", UserQuestion)


]