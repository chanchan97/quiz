from tornado_sqlalchemy import as_future
from model.store_info import Store_Info
from model.stat import Stat
from model.question import Question
from model.question_set import Question_Set

def get_store_info(self, user_id):
    info = self.db.query(Store_Info).filter(Store_Info.user_id == user_id).first()
    return info

def get_stat(self, user_id, question_set_id=None):
    info = self.db.query(Store_Info).filter(Store_Info.user_id == user_id).first()
    if info:
        stat_id = info.stat_id
        if question_set_id:
            stat = self.db.query(Stat).filter(Stat.id == stat_id).filter(Stat.question_set_id == question_set_id).first()
        else:   
            stat = self.db.query(Stat).filter(Stat.id == stat_id).first()
    else:
        stat = None
    return stat

def get_question_set(self, question_set_id):
    question_set = self.db.query(Question_Set).filter(Question_Set.id == question_set_id).first()
    return question_set

def get_question(self, question_id):
    question = self.db.query(Question).filter(Question.id == question_id).first()
    return question