import json
import wtforms_json
import tornado.web
from tornado_sqlalchemy import as_future

from model.question_set import Question_Set
from model.question import Question
from model.store_info import Store_Info
from model.answer import Answer
from uitls.exception import MyAppException
from basehandler import BaseHandler
from uitls.generate_json import to_json
from model.stat import Stat
from model.general_task import update
from uitls.hash_uitl import byte_to_uuid
from serivce.service import get_stat, get_store_info, get_question_set, get_question


class UserQuestion(BaseHandler):
    async def get(self, question_set_id, ordinal_number):
        current_user = self.get_current_user()
        if current_user:
            stat = get_stat(self, current_user['id'], question_set_id)
            question_set = await as_future(self.db.query(Question_Set).filter(Question_Set.id == question_set_id).first)
            questions = await as_future(self.db.query(Question).filter(Question.question_set_id == question_set.id).all)
            total = len(questions)
            question = self.db.query(Question).filter(Question.question_set_id == question_set_id).filter(Question.ordinal_number == ordinal_number).first()
            answers =  await as_future(self.db.query(Answer).filter(Answer.question_id == question.id).all)
            self.render('detail_question_set.html', question_set=question_set, question=question, answers=answers, total=total, stat=stat, error=None)
   
    async def post(self, question_set_id, ordinal_number):
        question = self.db.query(Question).filter(Question.question_set_id == question_set_id).filter(Question.ordinal_number == ordinal_number).first()
        current_user = self.get_current_user()
        stat = self.db.query(Stat).filter(Stat.question_set_id == question_set_id).first()
        info = get_store_info(self, current_user['id'])
        info_data = {}
        if stat:
            data_update = {}
            question_anwsered = get_question(self, stat.question_id)
            question_set = get_question_set(self, question_set_id)
            if question_anwsered.ordinal_number == int(question_set.number_of_questions):
                total = question_set.number_of_questions
                answers =  await as_future(self.db.query(Answer).filter(Answer.question_id == question.id).all)
                self.render('detail_question_set.html', question_set=question_set, question=question, answers=answers, total=total, stat=stat, error="You have already answered this question set so you cannot answer again.")
            else:
                if question.multiple:
                    right_value = []
                    values = self.request.arguments['anwser']
                    for value in values:
                        answer_id = byte_to_uuid(value)
                        answer = await as_future(self.db.query(Answer).filter(Answer.id == answer_id).first)
                        if answer.correct == 'True':
                            right_value.append(answer_id)
                    if len(right_value) > 1:
                        data_update['score'] = stat.score + 1
                        data_update['question_id'] = question.id
                    else:
                        data_update['question_id'] = question.id
                else:
                    value = self.get_argument('anwser')
                    answer = await as_future(self.db.query(Answer).filter(Answer.id == value).first)
                    if answer.correct == 'True':
                        data_update['score'] = stat.score + 1
                        data_update['question_id'] = question.id
                    else:
                        data_update['question_id'] = question.id
                update(stat, data_update, self.db)
                self.redirect("/"+ question_set_id + "/" + ordinal_number)
        else:
            if question.multiple:
                right_value = []
                values = self.request.arguments['anwser']
                for value in values:
                    answer_id = byte_to_uuid(value)
                    answer = await as_future(self.db.query(Answer).filter(Answer.id == answer_id).first)
                    if answer.correct == 'True':
                        right_value.append(answer_id)
                if len(right_value) > 1:
                    stat = Stat(user_id=self.current_user['id'], question_id=question.id, question_set_id=question_set_id, score=1)
                else:
                    stat = Stat(user_id=self.current_user['id'], question_id=question.id, question_set_id=question_set_id, score=0)
            else:
                value = self.get_argument('anwser')
                answer = await as_future(self.db.query(Answer).filter(Answer.id == value).first)
                if answer.correct == 'True':
                    stat = Stat(user_id=self.current_user['id'], question_id=question.id, question_set_id=question_set_id, score=1)
                else:
                    stat = Stat(user_id=self.current_user['id'], question_id=question.id, question_set_id=question_set_id, score=0)
            self.db.add(stat)
            self.db.commit()
            if info:
                #update info
                info_data['stat_id'] = stat.id
                update(stat, info_data, self.db)
            else:
                info = Store_Info(user_id=self.current_user['id'], stat_id=stat.id)
                self.db.add(info)
                self.db.commit()
            self.redirect("/"+ question_set_id + "/" + ordinal_number)
        