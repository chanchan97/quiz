import json
import wtforms_json
import tornado.web
from tornado_sqlalchemy import as_future

from model.user import User
from model.stat import Stat
from configure import UserRole
from basehandler import BaseHandler
from validator.validate import RegAcc, Login_Form
from uitls.generate_json import to_json
from uitls.hash_uitl import hash_password
from uitls.exception import MyAppException

class Register(BaseHandler):
    def get(self):
        self.render("register.html", error=None)


    async def post(self):
        # data = json.loads(self.request.body.decode('utf-8'))
        data = {
            'email': self.get_argument('email'),
            'user_name': self.get_argument('user_name'),
            'password': self.get_argument('password')
        }
        form = RegAcc.from_json(data)
        if form.validate():
            email = form.data['email']
            password =  hash_password(form.data["password"])
            user_name = form.data['user_name']
            email_existed = await as_future(self.db.query(User.email).filter(User.email == email).first)
            if email_existed:
                # raise MyAppException(reason='Email already in use', status_code=400)
                self.render("register.html", error='Email already in use')
            user = User(email=email, password=password, role=UserRole.USER, user_name=user_name)
            self.db.add(user)
            self.db.commit()
            # self.return_respone(data=to_json(user))
            self.set_secure_cookie("user_id", str(user.id))
            self.redirect("/home")
        else:
            # self.set_status(400)
            # self.write(form.errors)
            self.render("register.html", error=form.errors)


class Login(BaseHandler):
    
    def get(self):
        current_user = self.get_current_user()
        if not current_user:
            self.render("login.html", error=None)
        else:
            self.redirect("/home")

    async def post(self):
        if self.get_current_user():
           self.redirect("/home")
        else:
            # data = json.loads(self.request.body.decode('utf-8'))
            data = {
            'email': self.get_argument('email'),
            'password': self.get_argument('password')
        }
            form = Login_Form.from_json(data)
            if form.validate():
                email = form.data['email']
                password =  form.data["password"]
                user = await as_future(self.db.query(User).filter(User.email == email).first)
                if not user:
                    # raise MyAppException(reason='Not Found', status_code=400)
                    self.render("login.html", error="You're not found")
                if not user.check_password(password):
                    self.render("login.html", error="Your password was wrong")
                    # raise MyAppException(reason='Your password was wrong', status_code=401)
                self.set_secure_cookie("user_id", str(user.id))
                self.redirect("/home")
            else:
                # self.set_status(400)
                # self.write(form.errors)
                self.render("login.html", error=form.errors)

class AuthLogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user_id")
        self.redirect("/login")


