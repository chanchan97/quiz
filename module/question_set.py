import json
import wtforms_json
import tornado.web
from tornado_sqlalchemy import as_future

from model.question_set import Question_Set
from model.question import Question
from model.answer import Answer
from uitls.exception import MyAppException
from basehandler import BaseHandler
from uitls.generate_json import to_json


class QuestionSet(BaseHandler):
    def get(self):
        self.render("question_set_form.html")

    def post(self):
        question_set = Question_Set(title=self.get_argument('title'), number_of_questions=self.get_argument('number_of_questions'))
        self.db.add(question_set)
        self.db.commit()
        question_set_id = str(question_set.id)
        self.redirect("/add_question/" + question_set_id)

class DetailQuestionSet(BaseHandler):
    async def get(self, question_set_id):
        entries = []
        question_set = await as_future(self.db.query(Question_Set).filter(Question_Set.id == question_set_id).first)
        questions = self.db.query(Question).filter(Question.question_set_id == question_set.id).order_by(Question.ordinal_number.asc())
        for question in questions:
            question_dict = {}
            question_dict['question_title'] = question.text
            question_dict['ordinal_number'] = question.ordinal_number
            question_dict['image'] = question.image
            answers =  await as_future(self.db.query(Answer).filter(Answer.question_id == question.id).all)
            question_dict['answer'] = answers
            entries.append(question_dict)
        self.render('detail_question_set.html', question_set=question_set, entries=entries)
        

