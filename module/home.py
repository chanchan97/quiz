import json
import wtforms_json
import tornado.web
from tornado_sqlalchemy import as_future

from model.question_set import Question_Set
from model.question import Question
from model.answer import Answer
from uitls.exception import MyAppException
from basehandler import BaseHandler
from uitls.generate_json import to_json
from serivce.service import get_stat, get_question_set, get_question
from configure import UserRole


class QuestionSetHandler(BaseHandler):
    async def get(self):
        current_user = self.get_current_user()
        if not current_user:
            self.redirect("/login")
        stat = get_stat(self, current_user['id'])
        if stat and current_user['role'] == UserRole.USER:
            question_set = get_question_set(self, stat.question_set_id)
            question = get_question(self, stat.question_id)
            if question.ordinal_number == int(question_set.number_of_questions):
                questions_set = await as_future(self.db.query(Question_Set).all)
                entries = [to_json(question_set) for question_set in questions_set]
                self.render("home.html", entries=entries)
            else:
                self.redirect("/"+ str(question_set.id)+ "/" + str(question.ordinal_number + 1))
            
        else:
            questions_set = await as_future(self.db.query(Question_Set).all)
            entries = [to_json(question_set) for question_set in questions_set]
            self.render("home.html", entries=entries)

        