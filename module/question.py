import json
import wtforms_json
import tornado.web, os.path, random, string
from tornado_sqlalchemy import as_future

from model.question_set import Question_Set
from model.question import Question
from model.answer import Answer
from uitls.exception import MyAppException
from basehandler import BaseHandler
from uitls.generate_json import to_json


class Add(BaseHandler):
    def get(self, question_set_id):
        self.render("add_question.html", error=None)

    async def post(self, question_set_id):
        # data = json.loads(self.request.body.decode('utf-8'))
        question_set = await as_future(self.db.query(Question_Set).filter(Question_Set.id == question_set_id).first)
        questions = await as_future(self.db.query(Question).filter(Question.question_set_id == question_set.id).all)
        if len(questions) == int(question_set.number_of_questions):
            self.render("add_question.html", error="Number of questions doesn't exceed "+question_set.number_of_questions)
        else:
            if self.get_argument('useimage') == '1':
                image = self.request.files['image'][0]
                original_fname = image['filename']
                extension = os.path.splitext(original_fname)[1]
                fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(6))
                final_filename= fname+extension
                output_file = open("static/question_image/" + final_filename, 'wb')
                output_file.write(image['body'])
            else:
                final_filename = None

            if self.get_argument('multiple') == '1':
                question = Question(text=self.get_argument('question_text'), multiple=True , ordinal_number=self.get_argument('ordinal_number'), question_set_id=question_set_id, image=final_filename)
            else:
                question = Question(text=self.get_argument('question_text'), ordinal_number=self.get_argument('ordinal_number'), question_set_id=question_set_id, image=final_filename)
            self.db.add(question)
            self.db.commit()
            anwser1 = Answer(text=self.get_argument('text1'), correct=self.get_argument('correct1'), question_id=question.id)
            self.db.add(anwser1)
        
            anwser2 = Answer(text=self.get_argument('text2'), correct=self.get_argument('correct2'), question_id=question.id)
            self.db.add(anwser2)
            
            anwser3 = Answer(text=self.get_argument('text3'), correct=self.get_argument('correct3'), question_id=question.id)
            self.db.add(anwser3)
        
            anwser4 = Answer(text=self.get_argument('text4'), correct=self.get_argument('correct4'), question_id=question.id)
            self.db.add(anwser4)
            self.db.commit()
            self.redirect("/add_question/" + question_set_id)


