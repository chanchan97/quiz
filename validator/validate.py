from wtforms_tornado import Form
from wtforms import validators
from wtforms.validators import Required, Email, Regexp, Length
from wtforms.fields import PasswordField, StringField


EMAIL_PATTERN = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+$)"
PASSWORD_PATTERN = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)" \
                   r"(?=.*[!\"#$%&'()*+,-./:;<=>\?@\[\\\]^_`\{\|\}\~])" \
                   r"[A-Za-z\d!\"#$%&'()*+,-./:;<=>\?@\[\\\]^_`\{\|\}\~]{8,}$"


class RegAcc(Form):
    email = StringField(validators=[Required(), Email()])
    user_name = StringField(validators=[Required()])
    password = PasswordField(validators=[Required(), Length(min=8, max=35), Regexp(PASSWORD_PATTERN, message='Your password must contain letters,  at least one uppercase, at least one number digit, at least one special character')])


class Login_Form(Form):
    email = StringField(validators=[Required(), Email()])
    password = PasswordField(validators=[Required()])



