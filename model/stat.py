import uuid
from sqlalchemy import Column, String, ForeignKey, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from model.db import Base


class Stat(Base):
    __tablename__ = 'Stat'

    __json_public__ = [
        'id',
        'score',
        'user_id',
        'question_id',
        'question_set_id'
    ]

    __update_field__ = [
        'score',
        'user_id',
        'question_id',
        'question_set_id'
    ]

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    score = Column(Integer)
    user_id = Column(UUID(as_uuid=True), ForeignKey('user.id'))
    user = relationship('User')
    question_id = Column(UUID(as_uuid=True), ForeignKey('question.id'))
    question = relationship('Question')
    question_set_id = Column(UUID(as_uuid=True), ForeignKey('question_set.id'))
    question_set = relationship('Question_Set')


    def __init__(self, **kwargs):
        self.id = uuid.uuid4()
        super(Stat, self).__init__(**kwargs)