from uitls.generate_json import to_json


def update(obj, data_update, session, is_commit=True):
        fields_can_update = obj.__update_field__
        for k,v in list(data_update.items()):
                if v is None:
                    del data_update[k]
        for k, v in data_update.items():
            if k in fields_can_update:
                setattr(obj, k, v)
        try:
            if is_commit:
                session.commit()
        except Exception as error:
            session.rollback()
            raise error