from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine('postgres://thuy:thuy@127.0.0.1:5432/quizchoices',
                   encoding='utf-8', echo=False,
                   pool_size=100, pool_recycle=10)


Base = declarative_base()
