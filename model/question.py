import uuid
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from model.db import Base


class Question(Base):
    __tablename__ = 'question'

    __json_public__ = [
        'id',
        'text',
        'image',
        'ordinal_number',
        'question_set_id'
    ]

    __update_field__ = [
    ]

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    text = Column(String, nullable=False)
    image = Column(String, nullable=True)
    ordinal_number = Column(Integer)
    multiple = Column(Boolean, default=False)
    question_set_id = Column(UUID(as_uuid=True), ForeignKey('question_set.id'))
    question_set = relationship('Question_Set')


    def __init__(self, **kwargs):
        self.id = uuid.uuid4()
        super(Question, self).__init__(**kwargs)