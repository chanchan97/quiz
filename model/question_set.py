import uuid
from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import UUID
from model.db import Base


class Question_Set(Base):
    __tablename__ = 'question_set'

    __json_public__ = [
        'id',
        'title',
        'number_of_questions'
    ]

    __update_field__ = [
    ]

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    title = Column(String, nullable=False)
    number_of_questions = Column(String, nullable=True)


    def __init__(self, **kwargs):
        self.id = uuid.uuid4()
        super(Question_Set, self).__init__(**kwargs)