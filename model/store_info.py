import uuid
from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from model.db import Base


class Store_Info(Base):
    __tablename__ = 'store_info'

    __json_public__ = [
        'id',
        'stat_id',
        'user_id'
    ]

    __update_field__ = [
        'stat_id'
    ]

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    stat_id = Column(UUID(as_uuid=True), ForeignKey('Stat.id'))
    stat = relationship('Stat')
    user_id = Column(UUID(as_uuid=True), ForeignKey('user.id'))
    user = relationship('User')


    def __init__(self, **kwargs):
        self.id = uuid.uuid4()
        super(Store_Info, self).__init__(**kwargs)