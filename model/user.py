import uuid
from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import UUID


from uitls.hash_uitl import check_password
from model.db import Base


class User(Base):
    __tablename__ = 'user'

    __json_public__ = [
        'id',
        'email',
        'user_name',
        'role'
    ]

    __update_field__ = [
    ]

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    email = Column(String, nullable=False)
    user_name = Column(String)
    password =  Column(String)
    role = Column(String, nullable=False)

    def __init__(self, **kwargs):
        self.id = uuid.uuid4()
        super(User, self).__init__(**kwargs)

    def check_password(self, password):
        return check_password(self.password, password)