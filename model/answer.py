import uuid
from sqlalchemy import Column, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from model.db import Base


class Answer(Base):
    __tablename__ = 'answer'

    __json_public__ = [
        'id',
        'text',
        'correct',
        'question_id'
    ]

    __update_field__ = [
    ]

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    text = Column(String, nullable=False)
    correct = Column(String)
    question_id = Column(UUID(as_uuid=True), ForeignKey('question.id'))
    question = relationship('Question')


    def __init__(self, **kwargs):
        self.id = uuid.uuid4()
        super(Answer, self).__init__(**kwargs)