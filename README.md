## Prerequisites

* Python 3.7
* Postgres
* pip
* virtualenv

## Setup 
1. Create Environment
   python -m virtualenv .venv
2. Access virtual enviroment
   .\.venv\Scripts\activate
4. Install requirements
   pip install -r requirements.txt
5. Database
   Change to your database name, user and password
6. Run
   python main.py