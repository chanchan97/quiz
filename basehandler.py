import tornado.ioloop
import tornado.web
import json

from uitls.generate_json import generate_json, to_json
from uitls.exception import MyAppException
from model.user import User
from uitls.hash_uitl import byte_to_uuid

class BaseHandler(tornado.web.RequestHandler):


    @property
    def db(self):
        return self.application.db

    def set_default_headers(self, *args, **kwargs):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE")


    def return_respone(self, data=None,  status=200):
        self.set_status(status)
        result = {'success': True, 'data': data}
        self.write(result)


    def write_error(self, status_code, **kwargs):
        self.finish(json.dumps({
            'code': status_code,
            "success": False,
            'message': self._reason
    }))
    
    def get_current_user(self):
        cookie = self.get_secure_cookie("user_id")
        if not cookie:
           return None
        user_id = byte_to_uuid(cookie)
        current_user = to_json(self.db.query(User).filter(User.id == user_id).first())
        return current_user

    def get_stat_user(self):
        stat_id = self.get_secure_cookie("stat_id")
        if not stat_id:
            return None
        stat_id = byte_to_uuid(stat_id)
        return stat_id
        