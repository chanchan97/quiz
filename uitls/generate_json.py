import datetime
import datetime
import enum
import uuid


def generate_json(obj, deep=True):
    if hasattr(obj, "to_json") and deep:
        return obj.to_json()
    elif isinstance(obj, dict):
        for item in obj:
            obj[item] = generate_json(obj[item], deep=deep)
        return obj
    elif isinstance(obj, list):
        return [generate_json(element, deep=deep) for element in obj]
    elif isinstance(obj, datetime.datetime):
        return obj.isoformat()
    elif isinstance(obj, uuid.UUID):
        return str(obj)
    elif isinstance(obj, (int, float, bool)):
        return obj
    elif isinstance(obj, enum.Enum) and hasattr(obj, "name"):
        return obj.name
    elif obj is None:
        return None
    else:
        return str(obj)


def to_json(obj, deep=True):
    public = obj.__json_public__
    rv = dict()
    for key in public:
        rv[key] = getattr(obj, key)
    return generate_json(rv, deep=deep)