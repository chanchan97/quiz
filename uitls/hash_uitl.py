import hashlib
import hmac
import uuid


def hash_password(password):
    salt = uuid.uuid4().hex
    password_hashed = hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ':' + salt
    return password_hashed


def check_password(hashed_password, password):
    if (not hashed_password) or (not password):
        return False
    user_password, salt = hashed_password.split(":")
    return user_password == hashlib.sha256(salt.encode() + password.encode()).hexdigest()

def byte_to_uuid(value):
    value = (value).decode()
    value = uuid.UUID(uuid.UUID(value).hex)
    return value
